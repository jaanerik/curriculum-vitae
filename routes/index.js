var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', {
    title: 'Jaan Eriku CV Netgroupile',
  });
});

router.get('/motivation', function (req, res) {
    res.render('login', {})
});

router.post('/letter', function (req, res) {
    if (req.body.parool === 'netgroup'
        || req.body.parool === 'Netgroup'
        || req.body.parool === 'NetGroup'){
        res.redirect('/motivatsioonikiri.pdf');
    } else res.redirect('/');
});

module.exports = router;
