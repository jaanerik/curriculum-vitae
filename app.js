var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser')


var indexRouter = require('./routes/index');

var app = express();
// const basicAuth = require('express-basic-auth')

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


app.set('view engine', 'pug');
app.set('port', 3000);
console.log('Server is listening on port 3000');

app.use('/', indexRouter);

module.exports = app;
